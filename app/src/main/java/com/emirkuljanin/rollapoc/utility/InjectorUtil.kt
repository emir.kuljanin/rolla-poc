package com.emirkuljanin.rollapoc.utility

import android.app.Application
import com.emirkuljanin.rollapoc.data.FakeDatabase
import com.emirkuljanin.rollapoc.data.repository.ButtonItemRepository
import com.emirkuljanin.rollapoc.ui.buttontask.ButtonTaskViewModelFactory
import com.emirkuljanin.rollapoc.ui.castingtask.CastingTaskViewModelFactory

object InjectorUtil {

    fun provideButtonTaskViewModel(): ButtonTaskViewModelFactory {
        //ViewModelFactory needs a repository, which in turn needs a DAO from a database
        //The whole dependency tree is constructed right here
        val buttonTaskRepository = ButtonItemRepository.getInstance(
            FakeDatabase.getInstance().buttonItemDao
        )

        return ButtonTaskViewModelFactory(buttonTaskRepository)
    }

    fun provideCastingTaskViewModel(application: Application): CastingTaskViewModelFactory {
        return CastingTaskViewModelFactory(application)
    }
}