package com.emirkuljanin.rollapoc.utility

const val RESET_TAG = "Reset"
const val CAST_ID = "46F4D2A7"
const val READ_EXTERNAL_STORAGE_REQUEST_CODE = 911
const val SERVER_PORT = 12345
const val SERVER_VIDEO_ADDRESS_FORMAT = "http://%s:$SERVER_PORT"
const val DATA_COLUMN = "_data"
