package com.emirkuljanin.rollapoc.utility

import fi.iki.elonen.NanoHTTPD
import java.io.File
import java.io.FileInputStream
import java.io.FileNotFoundException

class FileHostServer(private val filePath: String) : NanoHTTPD(SERVER_PORT) {

    //Get file and send it as input stream
    override fun serve(
        uri: String?, method: Method?,
        header: Map<String?, String?>?, parameters: Map<String?, String?>?,
        files: Map<String?, String?>?
    ): Response? {
        val file = File(filePath)
        var fis: FileInputStream? = null
        try {
            fis = FileInputStream(file)
        } catch (e: FileNotFoundException) {
            e.printStackTrace()
        }

        return newFixedLengthResponse(
            Response.Status.OK, getMimeTypeForFile(filePath), fis,
            file.length()
        )
    }
}