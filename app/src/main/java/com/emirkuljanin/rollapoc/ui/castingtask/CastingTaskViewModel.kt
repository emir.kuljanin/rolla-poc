package com.emirkuljanin.rollapoc.ui.castingtask

import android.app.Application
import android.database.Cursor
import android.net.Uri
import android.provider.MediaStore
import androidx.lifecycle.AndroidViewModel
import androidx.mediarouter.media.MediaRouter
import com.emirkuljanin.rollapoc.utility.DATA_COLUMN
import com.google.android.gms.cast.CastDevice
import java.lang.ref.WeakReference

class CastingTaskViewModel(application: Application) : AndroidViewModel(application) {
    private val contextWeakReference =
        WeakReference(getApplication<Application>().applicationContext)

    fun getRoutes(): MutableList<MediaRouter.RouteInfo> {
        val context = contextWeakReference.get()
        val mediaRouter = MediaRouter.getInstance(context ?: return arrayListOf())
        val routes = mutableListOf<MediaRouter.RouteInfo>()

        //Go through list of routes and get those routes with a device in them
        mediaRouter.routes.forEach {
            val device = CastDevice.getFromBundle(it.extras)
            if (device != null) {
                routes.add(it)
            }
        }

        return routes
    }

    //Go through all videos and add their paths to a mutable list
    fun getAllVideos(): List<String> {
        val context = contextWeakReference.get()
        val uri: Uri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI
        val cursor: Cursor?
        val videoList = mutableListOf<String>()
        val column = DATA_COLUMN
        val projection = arrayOf(column)
        context ?: return arrayListOf()
        cursor = context.contentResolver
            .query(uri, projection, null, null, null)

        if (cursor != null) {
            while (cursor.moveToNext()) {
                val videoPath = cursor.getString(cursor.getColumnIndexOrThrow(DATA_COLUMN))

                videoList.add(videoPath)
            }
            cursor.close()
        }

        return videoList
    }
}