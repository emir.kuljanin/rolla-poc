package com.emirkuljanin.rollapoc.ui.castingtask.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.mediarouter.media.MediaRouter
import androidx.recyclerview.widget.RecyclerView
import com.emirkuljanin.rollapoc.databinding.CastDeviceRecyclerItemBinding
import com.google.android.gms.cast.CastDevice

class CastDevicesRecyclerAdapter(
    private val routeList: List<MediaRouter.RouteInfo>,
    private val context: Context
) : RecyclerView.Adapter<CastDevicesRecyclerAdapter.ViewHolder>() {
    private var lastCheckedPosition: Int = -1

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): CastDevicesRecyclerAdapter.ViewHolder {
        val itemBinding =
            CastDeviceRecyclerItemBinding.inflate(LayoutInflater.from(context), parent, false)

        return ViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: CastDevicesRecyclerAdapter.ViewHolder, position: Int) {
        holder.binding.isSelected = lastCheckedPosition == position

        //Get device name from routeInfo
        val device = CastDevice.getFromBundle(routeList[position].extras)
        holder.binding.deviceName = device.friendlyName
    }

    override fun getItemCount(): Int {
        return routeList.size
    }

    fun getSelectedItem(): MediaRouter.RouteInfo? {
        if (lastCheckedPosition == -1) return null
        return routeList[lastCheckedPosition]
    }

    inner class ViewHolder(val binding: CastDeviceRecyclerItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        init {
            //Handle selecting of item
            binding.root.setOnClickListener {
                //Notify change for previously selected item(If any was selected)
                if (lastCheckedPosition != -1) notifyItemChanged(lastCheckedPosition, false)
                //Update new position
                lastCheckedPosition = adapterPosition
                //Notify new change for selected item
                notifyItemChanged(lastCheckedPosition, true)
            }
        }
    }
}