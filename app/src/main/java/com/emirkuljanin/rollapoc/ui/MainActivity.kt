package com.emirkuljanin.rollapoc.ui

import android.Manifest
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.add
import androidx.fragment.app.commit
import com.emirkuljanin.rollapoc.R
import com.emirkuljanin.rollapoc.databinding.ActivityMainBinding
import com.emirkuljanin.rollapoc.ui.buttontask.ButtonTaskFragment
import com.emirkuljanin.rollapoc.ui.castingtask.CastingTaskFragment
import com.emirkuljanin.rollapoc.utility.READ_EXTERNAL_STORAGE_REQUEST_CODE
import com.google.android.gms.cast.framework.CastContext

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding: ActivityMainBinding =
            DataBindingUtil.setContentView(this, R.layout.activity_main)

        //Get cast context in order to get chromecast devices
        CastContext.getSharedInstance(this)

        binding.activity = this
    }

    //Handle orientation change (sometimes cast context was lost?)
    override fun onRestart() {
        super.onRestart()
        //Get cast context in order to get chromecast devices
        CastContext.getSharedInstance(this)
    }

    fun onButtonTaskClick() {
        supportFragmentManager.commit {
            setCustomAnimations(
                R.anim.slide_in,
                R.anim.slide_out
            )
            setReorderingAllowed(true)
            addToBackStack(ButtonTaskFragment().tag)
            add<ButtonTaskFragment>(R.id.fragment_container)
        }
    }

    fun onCastingTaskClick() {
        //Before opening ask for permission for external storage
        if (checkPermissionForReadExternalStorage()) {
            //Has permission, open fragment
            openCastingFragment()
        } else {
            //Does not have permission, ask for it
            requestPermissionForReadExternalStorage()
        }
    }

    private fun checkPermissionForReadExternalStorage(): Boolean {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val result = checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
            return result == PackageManager.PERMISSION_GRANTED
        }
        return false
    }

    private fun requestPermissionForReadExternalStorage() {
        ActivityCompat.requestPermissions(
            this, arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
            READ_EXTERNAL_STORAGE_REQUEST_CODE
        )
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        if (requestCode == READ_EXTERNAL_STORAGE_REQUEST_CODE) {
            if ((grantResults.isNotEmpty() &&
                        grantResults[0] == PackageManager.PERMISSION_GRANTED)
            ) {
                //Permission granted open fragment
                openCastingFragment()
            } else {
                //Permission not granted, show toast
                Toast.makeText(
                    this, getString(R.string.storage_error),
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    private fun openCastingFragment() {
        supportFragmentManager.commit {
            setCustomAnimations(
                R.anim.slide_in,
                R.anim.slide_out
            )
            setReorderingAllowed(true)
            addToBackStack(CastingTaskFragment().tag)
            add<CastingTaskFragment>(R.id.fragment_container)
        }
    }
}