package com.emirkuljanin.rollapoc.ui.buttontask

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.emirkuljanin.rollapoc.data.model.ButtonItem
import com.emirkuljanin.rollapoc.data.repository.ButtonItemRepository
import com.emirkuljanin.rollapoc.utility.RESET_TAG
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import java.util.*
import kotlin.random.Random

class ButtonTaskViewModel(private val buttonItemRepository: ButtonItemRepository) : ViewModel() {

    private var queue: Queue<String> = LinkedList<String>(listOf())
    private var queueRunning = false

    fun getButtons() = buttonItemRepository.getButtons()

    fun addButton(buttonItem: ButtonItem) = buttonItemRepository.addButton(buttonItem)

    //Handle button click
    fun onButtonClick(title: String) {
        //Add title to queue (if null it is reset option)
        queue.add(title)

        //If there is an item on the list, clear it
        if (!getButtons().value.isNullOrEmpty()) {
            removeAllButtons()
        }

        //Handle queue
        if (!queueRunning) {
            handleQueue()
        }
    }

    private fun removeAllButtons() {
        buttonItemRepository.removeAllButtons()
    }

    private fun handleQueue() {
        //Queue running do not run it again
        queueRunning = true
        //If queue is empty, reset boolean to false
        if (queue.isEmpty()) {
            queueRunning = false
            return
        }

        //Add random delay (1000ms to 3000ms)
        viewModelScope.launch {
            val delayNumber = Random.nextLong(1000, 3000)
            delay(delayNumber)

            //Get first button in queue
            val buttonTitle = queue.poll() ?: return@launch

            //If there is an item on the list, clear it
            if (!getButtons().value.isNullOrEmpty()) {
                removeAllButtons()
            }

            //Check if reset was clicked (null is reset option)
            if (buttonTitle == RESET_TAG) {
                removeAllButtons()
            } else {
                //Add button after delay
                addButton(ButtonItem(buttonTitle))
            }

            //If there are more elements in queue process them
            queueRunning = queue.isNotEmpty()
            if (queueRunning) {
                handleQueue()
            }
        }
    }
}