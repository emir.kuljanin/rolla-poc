package com.emirkuljanin.rollapoc.ui.buttontask

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.emirkuljanin.rollapoc.data.repository.ButtonItemRepository

class ButtonTaskViewModelFactory(private val buttonItemRepository: ButtonItemRepository) :
    ViewModelProvider.NewInstanceFactory() {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return ButtonTaskViewModel(buttonItemRepository) as T
    }
}