package com.emirkuljanin.rollapoc.ui.castingtask

import android.content.ContentResolver
import android.content.Context
import android.net.Uri
import android.net.wifi.WifiManager
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.MimeTypeMap
import android.widget.Toast
import androidx.core.content.getSystemService
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.mediarouter.media.MediaRouter
import com.emirkuljanin.rollapoc.R
import com.emirkuljanin.rollapoc.databinding.FragmentCastingTaskBinding
import com.emirkuljanin.rollapoc.ui.MainActivity
import com.emirkuljanin.rollapoc.ui.castingtask.adapter.CastDevicesRecyclerAdapter
import com.emirkuljanin.rollapoc.ui.castingtask.adapter.VideoRecyclerAdapter
import com.emirkuljanin.rollapoc.utility.FileHostServer
import com.emirkuljanin.rollapoc.utility.InjectorUtil
import com.emirkuljanin.rollapoc.utility.SERVER_VIDEO_ADDRESS_FORMAT
import com.google.android.gms.cast.CastStatusCodes
import com.google.android.gms.cast.MediaInfo
import com.google.android.gms.cast.MediaLoadRequestData
import com.google.android.gms.cast.framework.CastContext
import com.google.android.gms.cast.framework.Session
import com.google.android.gms.cast.framework.SessionManager
import com.google.android.gms.cast.framework.SessionManagerListener
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.util.*

class CastingTaskFragment : Fragment() {
    private lateinit var binding: FragmentCastingTaskBinding
    private lateinit var castAdapter: CastDevicesRecyclerAdapter
    private lateinit var videoAdapter: VideoRecyclerAdapter
    private lateinit var viewModel: CastingTaskViewModel
    private var server: FileHostServer? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_casting_task, container, false)

        initializeUI()

        return binding.root
    }

    private fun initializeUI() {
        val activity = activity ?: return
        val factory = InjectorUtil.provideCastingTaskViewModel(activity.application)
        //Create or get already created CastingTaskViewModel for this view
        viewModel = ViewModelProvider(this, factory)
            .get(CastingTaskViewModel::class.java)

        //Get list of cast devices from viewModel and add them to recycler
        val routes = viewModel.getRoutes()
        castAdapter = CastDevicesRecyclerAdapter(routes, activity)
        binding.chromecastRecycler.adapter = castAdapter

        //Get list of video files and add them to recycler
        val uiScope = CoroutineScope(Dispatchers.Main)
        uiScope.launch {
            //Load videos from a different thread
            val videoList = viewModel.getAllVideos()
            videoAdapter = VideoRecyclerAdapter(videoList, activity)
            binding.videoRecycler.adapter = videoAdapter
        }

        //Set play button onClick listener
        binding.playButton.setOnClickListener { castVideo() }

        binding.viewModel = viewModel
    }

    private fun castVideo() {
        val activity = activity as MainActivity

        //Get selected route from adapter
        val route = castAdapter.getSelectedItem()
        //Check if route has been selected
        if (route == null) {
            Toast.makeText(activity, getString(R.string.select_device_error), Toast.LENGTH_SHORT)
                .show()
            return
        }

        //Get selected video from adapter
        val videoPath = videoAdapter.getSelectedItem()
        //Check if video was selected
        if (videoPath == null) {
            Toast.makeText(activity, getString(R.string.select_video_error), Toast.LENGTH_SHORT)
                .show()
            return
        }

        //Create server
        if (server == null) {
            server = FileHostServer(videoPath)
        }

        //Check if server is started, if not then start it
        if (server?.isAlive == false) {
            server?.start()
        }

        //Cast video to selected route
        val mediaRouter: MediaRouter = MediaRouter.getInstance(activity)
        val context = CastContext.getSharedInstance(activity)
        val sessionManager: SessionManager = context.sessionManager

        //Connect to route
        mediaRouter.selectRoute(route)

        //Add session listener
        sessionManager.addSessionManagerListener(object : SessionManagerListener<Session?> {
            override fun onSessionStarted(p0: Session?, p1: String?) {
                Log.e("CASTING STARTED", "$p0")
                //Retrieve RemoteMediaClient from current cast session
                val castSession = sessionManager.currentCastSession
                val remoteMediaClient = castSession.remoteMediaClient
                val mimeType = getMimeType(Uri.parse(videoPath))

                //Create MediaInfo from video URI and load it on chromecast device
                //Load file from local server url (IP:12345)
                val mediaInfo = MediaInfo.Builder(
                    String.format(SERVER_VIDEO_ADDRESS_FORMAT, getWifiIp(activity))
                )
                    .setStreamType(MediaInfo.STREAM_TYPE_BUFFERED)
                    .setContentType(mimeType)
                    .build()

                //Send load request
                remoteMediaClient.load(
                    MediaLoadRequestData.Builder()
                        .setMediaInfo(mediaInfo)
                        .setAutoplay(true)
                        .build()
                )
            }

            override fun onSessionStarting(p0: Session?) {
                Log.e("CASTING STARTING", "$p0")
            }

            override fun onSessionStartFailed(p0: Session?, p1: Int) {
                val error = CastStatusCodes.getStatusCodeString(p1)
                Log.e("CASTING ERROR", "$error - $p0")
            }

            override fun onSessionEnding(p0: Session?) {
                Log.e("CASTING ENDING", "$p0")
            }

            override fun onSessionEnded(p0: Session?, p1: Int) {
                Log.e("CASTING ENDED", "$p0")
            }

            override fun onSessionResuming(p0: Session?, p1: String?) {
                Log.e("CASTING RESUMING", "$p0")
            }

            override fun onSessionResumed(p0: Session?, p1: Boolean) {
                Log.e("CASTING RESUMED", "$p0")
            }

            override fun onSessionResumeFailed(p0: Session?, p1: Int) {
                Log.e("CASTING FAILED", "$p0")
            }

            override fun onSessionSuspended(p0: Session?, p1: Int) {
                Log.e("CASTING SUSPENDED", "$p0")
            }
        })
    }

    //Get mimeType from file in order to pass it to casting device
    fun getMimeType(uri: Uri): String? {
        val mimeType: String?
        mimeType = if (ContentResolver.SCHEME_CONTENT == uri.scheme) {
            val cr: ContentResolver = activity?.applicationContext!!.contentResolver
            cr.getType(uri)
        } else {
            val fileExtension = MimeTypeMap.getFileExtensionFromUrl(
                uri
                    .toString()
            )
            MimeTypeMap.getSingleton().getMimeTypeFromExtension(
                fileExtension.toLowerCase(Locale.getDefault())
            )
        }
        return mimeType
    }

    //Get device IP or return error message
    private fun getWifiIp(context: Context): String {
        return context.getSystemService<WifiManager>().let {
            when {
                it == null -> "No wifi available"
                !it.isWifiEnabled -> "Wifi is disabled"
                it.connectionInfo == null -> "Wifi not connected"
                else -> {
                    val ip = it.connectionInfo.ipAddress
                    ((ip and 0xFF).toString() + "." + (ip shr 8 and 0xFF) + "." +
                            (ip shr 16 and 0xFF) + "." + (ip shr 24 and 0xFF))
                }
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        server?.stop()
    }
}