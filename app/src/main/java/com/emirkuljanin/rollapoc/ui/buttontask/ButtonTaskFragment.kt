package com.emirkuljanin.rollapoc.ui.buttontask

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.emirkuljanin.rollapoc.R
import com.emirkuljanin.rollapoc.data.model.ButtonItem
import com.emirkuljanin.rollapoc.databinding.ButtonItemBinding
import com.emirkuljanin.rollapoc.databinding.FragmentButtonTaskBinding
import com.emirkuljanin.rollapoc.ui.buttontask.adapter.ButtonRecyclerAdapter
import com.emirkuljanin.rollapoc.utility.InjectorUtil

class ButtonTaskFragment : Fragment() {
    private lateinit var binding: FragmentButtonTaskBinding
    private lateinit var viewModel: ButtonTaskViewModel
    private lateinit var adapter: ButtonRecyclerAdapter
    private lateinit var buttonList: List<ButtonItem>

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_button_task, container, false)

        initializeUI()

        return binding.root
    }

    private fun initializeUI() {
        val activity = activity ?: return
        val factory = InjectorUtil.provideButtonTaskViewModel()
        //Create or get already created ButtonTaskViewModel for this view
        viewModel = ViewModelProvider(this, factory)
            .get(ButtonTaskViewModel::class.java)

        binding.viewModel = viewModel

        //Add 8 buttons programmatically to the left side LinearLayout
        addButtons()

        buttonList = viewModel.getButtons().value ?: listOf()

        //Init adapter and set it to list
        adapter =
            ButtonRecyclerAdapter(
                buttonList,
                activity
            )
        binding.buttonRecycler.adapter = adapter

        //Observe list LiveData
        viewModel.getButtons().observe(activity, {
            //Check if item was added or deleted and notify adapter accordingly
            buttonList = it
            adapter.notifyDataSetChanged()
        })
    }

    private fun addButtons() {
        val activity = activity ?: return

        //Add 8 buttons, and set title based on index
        for (i in 1..8) {
            //Inflate button binding
            val buttonBinding = ButtonItemBinding.inflate(
                LayoutInflater.from(activity),
                binding.buttonHolder, true
            )

            //Set variables
            buttonBinding.title = String.format(getString(R.string.button_title), i.toString())
            buttonBinding.viewModel = viewModel
        }
    }

}