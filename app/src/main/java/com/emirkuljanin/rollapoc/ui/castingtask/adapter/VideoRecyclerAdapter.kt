package com.emirkuljanin.rollapoc.ui.castingtask.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.emirkuljanin.rollapoc.databinding.VideoRecyclerItemBinding

class VideoRecyclerAdapter(
    private val videoList: List<String>,
    private val context: Context
) : RecyclerView.Adapter<VideoRecyclerAdapter.ViewHolder>() {
    private var lastCheckedPosition: Int = -1

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): VideoRecyclerAdapter.ViewHolder {
        val itemBinding =
            VideoRecyclerItemBinding.inflate(LayoutInflater.from(context), parent, false)

        return ViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: VideoRecyclerAdapter.ViewHolder, position: Int) {
        holder.binding.isSelected = lastCheckedPosition == position

        Glide.with(context).load(videoList[position])
            .into(holder.binding.thumbnailImageView)
    }

    override fun getItemCount(): Int {
        return videoList.size
    }

    fun getSelectedItem(): String? {
        if (lastCheckedPosition == -1) return null
        return videoList[lastCheckedPosition]
    }

    inner class ViewHolder(val binding: VideoRecyclerItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        init {
            //Handle selecting of item
            binding.root.setOnClickListener {
                //Notify change for previously selected item(If any was selected)
                if (lastCheckedPosition != -1) notifyItemChanged(lastCheckedPosition, false)
                //Update new position
                lastCheckedPosition = adapterPosition
                //Notify new change for selected item
                notifyItemChanged(lastCheckedPosition, true)
            }
        }
    }
}