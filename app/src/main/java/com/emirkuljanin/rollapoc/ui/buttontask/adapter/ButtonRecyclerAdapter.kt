package com.emirkuljanin.rollapoc.ui.buttontask.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.emirkuljanin.rollapoc.data.model.ButtonItem
import com.emirkuljanin.rollapoc.databinding.ButtonRecyclerItemBinding

class ButtonRecyclerAdapter(
    private val buttonsList: List<ButtonItem>,
    private val context: Context
) : RecyclerView.Adapter<ButtonRecyclerAdapter.ViewHolder>() {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        val itemBinding =
            ButtonRecyclerItemBinding.inflate(LayoutInflater.from(context), parent, false)

        return ViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.binding.buttonText = buttonsList[position].title
    }

    override fun getItemCount(): Int {
        return buttonsList.size
    }

    inner class ViewHolder(val binding: ButtonRecyclerItemBinding) :
        RecyclerView.ViewHolder(binding.root)
}