package com.emirkuljanin.rollapoc.data.dao

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.emirkuljanin.rollapoc.data.model.ButtonItem

class FakeButtonItemDao {
    //Create fake database table (in this case it will be cached)
    private val buttonList = mutableListOf<ButtonItem>()

    //Create observable LiveData
    private val buttons = MutableLiveData<List<ButtonItem>>()

    init {
        //Connect the empty buttonList with the observable buttons list
        buttons.value = buttonList
    }

    fun addButton(buttonItem: ButtonItem) {
        buttonList.add(buttonItem)
        //After a button is added to the fake database (cache) update the live data value
        //which will notify all active observers
        buttons.value = buttonList
    }

    fun removeButton(buttonItem: ButtonItem) {
        buttonList.remove(buttonItem)
        //After a button is removed from the fake database (cache) update the live data value
        //which will notify all active observers
        buttons.value = buttonList
    }

    fun removeAllButtons() {
        buttonList.clear()
        //After all buttons are removed from the fake database (cache) update the live data value
        //which will notify all active observers
        buttons.value = buttonList
    }

    //Cast to LiveData because the value of buttons should not be changed from other classes
    fun getButtons() = buttons as LiveData<List<ButtonItem>>
}