package com.emirkuljanin.rollapoc.data

import com.emirkuljanin.rollapoc.data.dao.FakeButtonItemDao

//Did not make this as an object because most databases require constructors with parameters
//Go with java singleton implementation
class FakeDatabase private constructor() {

    var buttonItemDao = FakeButtonItemDao()
        private set

    companion object {
        //Volatile - because writes to this property are immediately visible to all threads
        @Volatile private var instance: FakeDatabase? = null

        fun getInstance() =
            //If already instantiated return instance
            //If not instantiate it in a thread safe manner
            instance ?: synchronized(this) {
                //If it's still not instantiated create an object
                //Set instance property to be the created one
                instance ?: FakeDatabase().also { instance = it }
            }
    }
}