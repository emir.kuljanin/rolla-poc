package com.emirkuljanin.rollapoc.data.repository

import com.emirkuljanin.rollapoc.data.model.ButtonItem
import com.emirkuljanin.rollapoc.data.dao.FakeButtonItemDao

//Pass FakeButtonItemDao in order to be able to pass a mocked DAO to the repository
class ButtonItemRepository private constructor(private val buttonItemDao: FakeButtonItemDao) {

    fun addButton(buttonItem: ButtonItem) {
        //Check if item should be added or removed
        val buttonList: List<ButtonItem> = buttonItemDao.getButtons().value ?: listOf()

        if (buttonList.contains(buttonItem)) {
            buttonItemDao.removeButton(buttonItem)
        } else {
            buttonItemDao.addButton(buttonItem)
        }
    }

    fun removeAllButtons() {
        buttonItemDao.removeAllButtons()
    }

    fun getButtons() = buttonItemDao.getButtons()

    //Same singleton implementation as in the database
    companion object {
        @Volatile private var instance: ButtonItemRepository? = null

        fun getInstance(quoteDao: FakeButtonItemDao) =
            instance ?: synchronized(this) {
                instance ?: ButtonItemRepository(quoteDao).also { instance = it }
            }
    }
}