package com.emirkuljanin.rollapoc.data.model

data class ButtonItem(val title: String)